package layout_cotefacil

import (
	arquivoPedido57 "bitbucket.org/infarma/layout-cotefacil/v_5_7/arquivoDePedido"
	arquivoPedido60 "bitbucket.org/infarma/layout-cotefacil/v_6_0/arquivoDePedido"
	"os"
)

func GetArquivoDePedido57(fileHandle *os.File) (arquivoPedido57.ArquivoDePedido, error) {
	return arquivoPedido57.GetStruct(fileHandle)
}

func GetArquivoDePedido60(fileHandle *os.File) (arquivoPedido60.ArquivoDePedido, error) {
	return arquivoPedido60.GetStruct(fileHandle)
}
