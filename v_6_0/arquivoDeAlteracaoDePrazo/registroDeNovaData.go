package posicoesArquivoDeAlteracaoDePrazo

// Registro De Cabecalho
type PosicoesRegistroDeNovaData struct {
	TipoDeRegistro string `json:"TipoDeRegistro"`
	NovaData       int32  `json:"NovaData"`
}

