package posicoesArquivoDeAlteracaoDePrazo


// Registro De Produtos
type RegistroDeRodape struct {
	TipoDeRegistro string `json:"TipoDeRegistro"`
	VersaoDoLayout string `json:"VersaoDoLayout"`
}

func (r *RegistroDeRodape) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesArquivoDeAlteracaoDePrazo.PosicoesRegistroDeRodape

	err = posicaoParaValor.ReturnByType(&r.TipoDeRegistro, "TipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.VersaoDoLayout, "VersaoDoLayout")

	return err
}
