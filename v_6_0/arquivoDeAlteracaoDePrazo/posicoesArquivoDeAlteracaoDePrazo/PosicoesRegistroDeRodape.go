package posicoesArquivoDeAlteracaoDePrazo

import "layout-cotefacil/posicoes"

var PosicoesRegistroDeRodape = map[string]posicoes.Posicao{
	"TipoDeRegistro":       {0, 1},
	"VersaoDoLayout":       {2, 5},
}