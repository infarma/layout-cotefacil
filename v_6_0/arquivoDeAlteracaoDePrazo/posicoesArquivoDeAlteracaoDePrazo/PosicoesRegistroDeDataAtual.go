package posicoesArquivoDeAlteracaoDePrazo

import "layout-cotefacil/posicoes"

var PosicoesRegistroDeDataAtual = map[string]posicoes.Posicao{
	"TipoDeRegistro": {0, 1},
	"DataAtual":      {2, 15},
}
