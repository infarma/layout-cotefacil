package posicoesArquivoDeAlteracaoDePrazo

import "layout-cotefacil/posicoes"

var PosicoesRegistroDeCabecalho = map[string]posicoes.Posicao{
	"TipoDeRegistro":                 {0, 1},
	"CodigoDeCotacao":                {2, 12},
	"CNPJ":                           {13, 27},
}
