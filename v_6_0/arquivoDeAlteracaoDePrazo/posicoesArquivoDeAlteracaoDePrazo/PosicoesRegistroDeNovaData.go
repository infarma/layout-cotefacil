package posicoesArquivoDeAlteracaoDePrazo

import "layout-cotefacil/posicoes"

var PosicoesRegistroDeNovaData = map[string]posicoes.Posicao{
	"TipoDeRegistro": {0, 1},
	"NovaData":      {2, 15},
}
