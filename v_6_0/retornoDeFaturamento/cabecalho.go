package retornoDeFaturamento

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Cabecalho struct {
	TipoRegistro            int32  `json:"TipoRegistro"`
	CnpjFornecedor          string `json:"CnpjFornecedor"`
	CodigoCotacao           string `json:"CodigoCotacao"`
	AprovacaoPedido         string `json:"AprovacaoPedido"`
	MotivoNaoFaturamento    int32  `json:"MotivoNaoFaturamento"`
	CodigoPedido            string `json:"CodigoPedido"`
	CodigoCondicaoPagamento string `json:"CodigoCondicaoPagamento"`
}

func (c *Cabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalho

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjFornecedor, "CnpjFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoCotacao, "CodigoCotacao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.AprovacaoPedido, "AprovacaoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.MotivoNaoFaturamento, "MotivoNaoFaturamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoPedido, "CodigoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoCondicaoPagamento, "CodigoCondicaoPagamento")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":            {0, 1, 0},
	"CnpjFornecedor":          {1, 15, 0},
	"CodigoCotacao":           {15, 34, 0},
	"AprovacaoPedido":         {34, 35, 0},
	"MotivoNaoFaturamento":    {35, 37, 0},
	"CodigoPedido":            {37, 56, 0},
	"CodigoCondicaoPagamento": {56, 71, 0},
}
