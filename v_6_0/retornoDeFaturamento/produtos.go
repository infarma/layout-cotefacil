package retornoDeFaturamento

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Produtos struct {
	TipoRegistro             int32   `json:"TipoRegistro"`
	CodigoEanDunProduto      string  `json:"CodigoEanDunProduto"`
	QuantidadeFaturada       int32   `json:"QuantidadeFaturada"`
	PrecoComST               float32 `json:"PrecoComST"`
	MotivoNaoFaturamenteItem int32   `json:"MotivoNaoFaturamenteItem"`
	PrecoSemST               float32 `json:"PrecoSemST"`
}

func (p *Produtos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesProdutos

	err = posicaoParaValor.ReturnByType(&p.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CodigoEanDunProduto, "CodigoEanDunProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.QuantidadeFaturada, "QuantidadeFaturada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.PrecoComST, "PrecoComST")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.MotivoNaoFaturamenteItem, "MotivoNaoFaturamenteItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.PrecoSemST, "PrecoSemST")
	if err != nil {
		return err
	}

	return err
}

var PosicoesProdutos = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":             {0, 1, 0},
	"CodigoEanDunProduto":      {1, 15, 0},
	"QuantidadeFaturada":       {15, 22, 0},
	"PrecoComST":               {22, 30, 2},
	"MotivoNaoFaturamenteItem": {30, 32, 0},
	"PrecoSemST":               {30, 38, 2},
}
