package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Produtos struct {
	TipoRegistro       int32  	`json:"TipoRegistro"`
	CodigoBarras       string 	`json:"CodigoBarras"`
	QuantidadeFaturada int64  	`json:"QuantidadeFaturada"`
	PrecoComST         float32	`json:"PrecoComST"`
	Motivo             int32  	`json:"Motivo"`
	PrecoSemST         float32	`json:"PrecoSemST"`
}

func (p *Produtos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesProdutos

	err = posicaoParaValor.ReturnByType(&p.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CodigoBarras, "CodigoBarras")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.QuantidadeFaturada, "QuantidadeFaturada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.PrecoComST, "PrecoComST")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.Motivo, "Motivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.PrecoSemST, "PrecoSemST")
	if err != nil {
		return err
	}


	return err
}

var PosicoesProdutos = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 1, 0},
	"CodigoBarras":                      {1, 15, 0},
	"QuantidadeFaturada":                      {15, 22, 0},
	"PrecoComST":                      {22, 30, 2},
	"Motivo":                      {30, 32, 0},
	"PrecoSemST":                      {32, 40, 2},
}