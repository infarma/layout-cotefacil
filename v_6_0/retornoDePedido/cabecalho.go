package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Cabecalho struct {
	TipoRegistro     int32 	`json:"TipoRegistro"`
	Cnpj             string	`json:"Cnpj"`
	CodigoReferencia int64 	`json:"CodigoReferencia"`
	Faturado         string	`json:"Faturado"`
	Motivo           int32 	`json:"Motivo"`
	CodigoPedido     string	`json:"CodigoPedido"`
	PrazoPagamento   int32 	`json:"PrazoPagamento"`
}

func (c *Cabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalho

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.Cnpj, "Cnpj")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoReferencia, "CodigoReferencia")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.Faturado, "Faturado")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.Motivo, "Motivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoPedido, "CodigoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PrazoPagamento, "PrazoPagamento")
	if err != nil {
		return err
	}


	return err
}

var PosicoesCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 1, 0},
	"Cnpj":                      {1, 15, 0},
	"CodigoReferencia":                      {15, 25, 0},
	"Faturado":                      {25, 26, 0},
	"Motivo":                      {26, 29, 0},
	"CodigoPedido":                      {29, 39, 0},
	"PrazoPagamento":                      {39, 42, 0},
}