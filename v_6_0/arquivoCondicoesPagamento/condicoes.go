package arquivoCondicoesPagamento

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Condicoes struct {
	TipoRegistro            int32  `json:"TipoRegistro"`
	CnpjCliente             int64  `json:"CnpjCliente"`
	CodigoCondicaoPagamento string `json:"CodigoCondicaoPagamento"`
	NumeroDias              string `json:"NumeroDias"`
}

func (c *Condicoes) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCondicoes

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjCliente, "CnpjCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoCondicaoPagamento, "CodigoCondicaoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroDias, "NumeroDias")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCondicoes = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":            {0, 1, 0},
	"CnpjCliente":             {1, 15, 0},
	"CodigoCondicaoPagamento": {15, 115, 0},
	"NumeroDias":              {115, 215, 0},
}
