package remessaCotacao

// Registro De Produtos
type RegistroDeProdutos struct {
	TipoDeRegistro  string `json:"TipoDeRegistro"`
	CodigoDeBarras  string `json:"CodigoDeBarras"`
	Quantidade      int32  `json:"Quantidade"`
	ValorReferencia int32  `json:"ValorReferencia"`
}

func (r *RegistroDeProdutos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesRemessaCotacao.PosicoesRegistroDeProdutos

	err = posicaoParaValor.ReturnByType(&r.TipoDeRegistro, "TipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.CodigoDeBarras, "CodigoDeBarras")
	err = posicaoParaValor.ReturnByType(&r.Quantidade, "Quantidade")
	err = posicaoParaValor.ReturnByType(&r.ValorReferencia, "ValorReferencia")

	return err
}
