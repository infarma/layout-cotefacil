package posicoesRemessaCotacao

import "layout-cotefacil/posicoes"

var PosicoesRegistroDeCotacaoUnificada = map[string]posicoes.Posicao{
	"TipoDeRegistro":      {0, 1},
	"CNPJ":                {2, 16},
	"UF":                  {17, 19},
	"CodigoDeFaturamento": {20, 35},
}