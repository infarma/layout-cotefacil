package posicoesRemessaCotacao

import "layout-cotefacil/posicoes"

var PosicoesRegistroDeCabecalho = map[string]posicoes.Posicao{
	"TipoDeRegistro":                 {0, 1},
	"CNPJ":                           {1, 15},
	"UF":                             {15, 17},
	"DataDeEncerramento":             {17, 30},
	"CodigoDeCotacao":                {30, 49},
	"FormaDePagamento":               {49, 50},
	"CodigoDoCliente":                {50, 65},
	"DataInicialDeValidadeDosPrecos": {65, 73},
	"DataFinalDeValidadeDosPrecos":   {73, 81},
	"CodigoCondicaoPagamento":        {81, 96},
	"CnpjFornecedor":                 {96, 110},
}
