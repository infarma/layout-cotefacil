package remessaCotacao

// Registro De Produtos
type RegistroDeRodape struct {
	TipoDeRegistro       string `json:"TipoDeRegistro"`
	QuantidadeDeProdutos int32  `json:"QuantidadeDeProdutos"`
	VersaoDoLayout       string `json:"VersaoDoLayout"`
}

