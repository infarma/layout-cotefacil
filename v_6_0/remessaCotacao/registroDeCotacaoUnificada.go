package remessaCotacao

type RegistroDeCotacaoUnificada struct {
	TipoDeRegistro      string `json:"TipoDeRegistro"`
	CNPJ                string `json:"CNPJ"`
	UF                  string `json:"UF"`
	CodigoDeFaturamento int32  `json:"CodigoDeFaturamento"`
}

