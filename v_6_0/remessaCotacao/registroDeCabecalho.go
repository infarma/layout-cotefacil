package remessaCotacao

// Registro De Cabecalho
type RegistroDeCabecalho struct {
	TipoDeRegistro                 string `json:"TipoDeRegistro"`
	CNPJ                           string `json:"CNPJ"`
	UF                             string `json:"UF"`
	DataDeEncerramento             int32  `json:"DataDeEncerramento"`
	CodigoDeCotacao                int32  `json:"CodigoDeCotacao"`
	FormaDePagamento               string `json:"FormaDePagamento"`
	CodigoDoCliente                int32  `json:"CodigoDoCliente"`
	DataInicialDeValidadeDosPrecos int32  `json:"DataInicialDeValidadeDosPrecos"`
	DataFinalDeValidadeDosPrecos   int32  `json:"DataFinalDeValidadeDosPrecos"`
	CodigoCondicaoPagamento        string `json:"CodigoCondicaoPagamento"`
	CnpjFornecedor                 int32  `json:"CnpjFornecedor"`
}

func (r *RegistroDeCabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesRemessaCotacao.PosicoesRegistroDeCabecalho

	err = posicaoParaValor.ReturnByType(&r.TipoDeRegistro, "TipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.CNPJ, "CNPJ")
	err = posicaoParaValor.ReturnByType(&r.UF, "UF")
	err = posicaoParaValor.ReturnByType(&r.DataDeEncerramento, "DataDeEncerramento")
	err = posicaoParaValor.ReturnByType(&r.CodigoDeCotacao, "CodigoDeCotacao")
	err = posicaoParaValor.ReturnByType(&r.FormaDePagamento, "FormaDePagamento")
	err = posicaoParaValor.ReturnByType(&r.CodigoDoCliente, "CodigoDoCliente")
	err = posicaoParaValor.ReturnByType(&r.DataInicialDeValidadeDosPrecos, "DataInicialDeValidadeDosPrecos")
	err = posicaoParaValor.ReturnByType(&r.DataFinalDeValidadeDosPrecos, "DataFinalDeValidadeDosPrecos")
	err = posicaoParaValor.ReturnByType(&r.CodigoCondicaoPagamento, "CodigoCondicaoPagamento")
	err = posicaoParaValor.ReturnByType(&r.CnpjFornecedor, "CnpjFornecedor")

	return err
}
