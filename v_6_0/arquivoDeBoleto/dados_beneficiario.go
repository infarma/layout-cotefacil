package arquivoDeBoleto

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type DadosBeneficiario struct {
	TipoRegistro       int32  `json:"TipoRegistro"`
	Cnpj               int64  `json:"Cnpj"`
	Nome               string `json:"Nome"`
	CodigoBeneficiario string `json:"CodigoBeneficiario"`
}

func (d *DadosBeneficiario) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDadosBeneficiario

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Cnpj, "Cnpj")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Nome, "Nome")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoBeneficiario, "CodigoBeneficiario")
	if err != nil {
		return err
	}

	return err
}

var PosicoesDadosBeneficiario = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":       {0, 1, 0},
	"Cnpj":               {1, 15, 0},
	"Nome":               {15, 115, 0},
	"CodigoBeneficiario": {115, 165, 0},
}
