package arquivoDeBoleto

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type DadosPagador struct {
	TipoRegistro int32 	`json:"TipoRegistro"`
	CnpjCpf      int64 	`json:"CnpjCpf"`
	Nome         string	`json:"Nome"`
	TipoPessoa   string	`json:"TipoPessoa"`
}

func (d *DadosPagador) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDadosPagador

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CnpjCpf, "CnpjCpf")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Nome, "Nome")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.TipoPessoa, "TipoPessoa")
	if err != nil {
		return err
	}


	return err
}

var PosicoesDadosPagador = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 1, 0},
	"CnpjCpf":                      {1, 15, 0},
	"Nome":                      {15, 115, 0},
	"TipoPessoa":                      {115, 165, 0},
}