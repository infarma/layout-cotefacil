package arquivoDeBoleto

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type EnderecoPagador struct {
	TipoRegistro int32  `json:"TipoRegistro"`
	Endereco     string `json:"Endereco"`
	Bairro       string `json:"Bairro"`
	CEP          int32  `json:"CEP"`
	Municipio    string `json:"Municipio"`
	UF           string `json:"UF"`
}

func (e *EnderecoPagador) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesEnderecoPagador

	err = posicaoParaValor.ReturnByType(&e.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.Endereco, "Endereco")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.Bairro, "Bairro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.CEP, "CEP")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.Municipio, "Municipio")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&e.UF, "UF")
	if err != nil {
		return err
	}

	return err
}

var PosicoesEnderecoPagador = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro": {0, 1, 0},
	"Endereco":     {1, 101, 0},
	"Bairro":       {101, 151, 0},
	"CEP":          {151, 159, 0},
	"Municipio":    {159, 209, 0},
	"UF":           {209, 211, 0},
}
