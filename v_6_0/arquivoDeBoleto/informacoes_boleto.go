package arquivoDeBoleto

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type InformacoesBoleto struct {
	TipoRegistro  int32  `json:"TipoRegistro"`
	VersaoArquivo string `json:"VersaoArquivo"`
}

func (i *InformacoesBoleto) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesInformacoesBoleto

	err = posicaoParaValor.ReturnByType(&i.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.VersaoArquivo, "VersaoArquivo")
	if err != nil {
		return err
	}

	return err
}

var PosicoesInformacoesBoleto = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":  {0, 1, 0},
	"VersaoArquivo": {1, 4, 0},
}
