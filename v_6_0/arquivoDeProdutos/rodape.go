package arquivoDeProdutos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Rodape struct {
	TipoRegistro            int32 	`json:"TipoRegistro"`
	QuantidadeTotalProdutos int32 	`json:"QuantidadeTotalProdutos"`
	VersaoArquivo           string	`json:"VersaoArquivo"`
}

func (r *Rodape) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRodape

	err = posicaoParaValor.ReturnByType(&r.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeTotalProdutos, "QuantidadeTotalProdutos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.VersaoArquivo, "VersaoArquivo")
	if err != nil {
		return err
	}


	return err
}

var PosicoesRodape = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 1, 0},
	"QuantidadeTotalProdutos":                      {1, 7, 0},
	"VersaoArquivo":                      {7, 10, 0},
}