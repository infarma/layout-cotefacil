package arquivoDeProdutos

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Produtos struct {
	TipoRegistro         int32  `json:"TipoRegistro"`
	CodigoBarras         string `json:"CodigoBarras"`
	CodigoProduto        string `json:"CodigoProduto"`
	NomeProduto          string `json:"NomeProduto"`
	NomeFabricante       string `json:"NomeFabricante"`
	SituacaoProduto      int32  `json:"SituacaoProduto"`
	CategoriaProduto     string `json:"CategoriaProduto"`
	Subcategoria1Produto string `json:"Subcategoria1Produto"`
	Subcategoria2Produto string `json:"Subcategoria2Produto"`
	Subcategoria3Produto string `json:"Subcategoria3Produto"`
	FormatoVendaProduto  string `json:"FormatoVendaProduto"`
	QuantidadePorCaixa   int32  `json:"QuantidadePorCaixa"`
}

func (p *Produtos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesProdutos

	err = posicaoParaValor.ReturnByType(&p.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CodigoBarras, "CodigoBarras")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CodigoProduto, "CodigoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.NomeProduto, "NomeProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.NomeFabricante, "NomeFabricante")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.SituacaoProduto, "SituacaoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CategoriaProduto, "CategoriaProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.Subcategoria1Produto, "Subcategoria1Produto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.Subcategoria2Produto, "Subcategoria2Produto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.Subcategoria3Produto, "Subcategoria3Produto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.FormatoVendaProduto, "FormatoVendaProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.QuantidadePorCaixa, "QuantidadePorCaixa")
	if err != nil {
		return err
	}

	return err
}

var PosicoesProdutos = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":         {0, 1, 0},
	"CodigoBarras":         {1, 15, 0},
	"CodigoProduto":        {15, 23, 0},
	"NomeProduto":          {23, 123, 0},
	"NomeFabricante":       {123, 173, 0},
	"SituacaoProduto":      {173, 174, 0},
	"CategoriaProduto":     {174, 224, 0},
	"Subcategoria1Produto": {224, 274, 0},
	"Subcategoria2Produto": {274, 324, 0},
	"Subcategoria3Produto": {324, 374, 0},
	"FormatoVendaProduto":  {374, 375, 0},
	"QuantidadePorCaixa":   {375, 380, 0},
}
