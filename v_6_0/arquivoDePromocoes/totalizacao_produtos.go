package arquivoDePromocoes

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type TotalizacaoProdutos struct {
	TipoRegistro       int32 `json:"TipoRegistro"`
	QuantidadeProdutos int32 `json:"QuantidadeProdutos"`
}

func (t *TotalizacaoProdutos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesTotalizacaoProdutos

	err = posicaoParaValor.ReturnByType(&t.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.QuantidadeProdutos, "QuantidadeProdutos")
	if err != nil {
		return err
	}

	return err
}

var PosicoesTotalizacaoProdutos = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":       {0, 1, 0},
	"QuantidadeProdutos": {1, 7, 0},
}
