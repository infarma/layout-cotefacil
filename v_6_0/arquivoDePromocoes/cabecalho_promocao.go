package arquivoDePromocoes

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type CabecalhoPromocao struct {
	TipoRegistro     int32   `json:"TipoRegistro"`
	CodigoPromocao   string  `json:"CodigoPromocao"`
	MininoUnidade    int32   `json:"MininoUnidade"`
	ValorMinimo      float32 `json:"ValorMinimo"`
	StatusPromocao   int32   `json:"StatusPromocao"`
	ValidadePromocao string  `json:"ValidadePromocao"`
}

func (c *CabecalhoPromocao) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalhoPromocao

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoPromocao, "CodigoPromocao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.MininoUnidade, "MininoUnidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ValorMinimo, "ValorMinimo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.StatusPromocao, "StatusPromocao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ValidadePromocao, "ValidadePromocao")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCabecalhoPromocao = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":     {0, 1, 0},
	"CodigoPromocao":   {1, 101, 0},
	"MininoUnidade":    {101, 107, 0},
	"ValorMinimo":      {107, 115, 2},
	"StatusPromocao":   {115, 116, 0},
	"ValidadePromocao": {116, 124, 0},
}
