package arquivoDePromocoes

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type PrazosValidos struct {
	TipoRegistro int32  `json:"TipoRegistro"`
	CodigoPrazo  string `json:"CodigoPrazo"`
}

func (p *PrazosValidos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesPrazosValidos

	err = posicaoParaValor.ReturnByType(&p.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CodigoPrazo, "CodigoPrazo")
	if err != nil {
		return err
	}

	return err
}

var PosicoesPrazosValidos = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro": {0, 1, 0},
	"CodigoPrazo":  {1, 101, 0},
}
