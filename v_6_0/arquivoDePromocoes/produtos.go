package arquivoDePromocoes

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Produtos struct {
	TipoRegistro                  int32   `json:"TipoRegistro"`
	CodigoBarras                  string  `json:"CodigoBarras"`
	CodigoProduto                 string  `json:"CodigoProduto"`
	ValorComST                    float32 `json:"ValorComST"`
	Desconto                      float32 `json:"Desconto"`
	FormatoVendaProduto           string  `json:"FormatoVendaProduto"`
	QuantidadeCaixa               int32   `json:"QuantidadeCaixa"`
	MinimoUnidades                int32   `json:"MinimoUnidades"`
	ValorMinimo                   float32 `json:"ValorMinimo"`
	DescontoAdiconalOL            float32 `json:"DescontoAdiconalOL"`
	DescontoFinanceiro            float32 `json:"DescontoFinanceiro"`
	DescontoBonificadoVerba       float32 `json:"DescontoBonificadoVerba"`
	QuantidadeUnidadesBonificadas string  `json:"QuantidadeUnidadesBonificadas"`
	PorcentagemRepasse            float32 `json:"PorcentagemRepasse"`
}

func (p *Produtos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesProdutos

	err = posicaoParaValor.ReturnByType(&p.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CodigoBarras, "CodigoBarras")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CodigoProduto, "CodigoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.ValorComST, "ValorComST")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.Desconto, "Desconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.FormatoVendaProduto, "FormatoVendaProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.QuantidadeCaixa, "QuantidadeCaixa")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.MinimoUnidades, "MinimoUnidades")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.ValorMinimo, "ValorMinimo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.DescontoAdiconalOL, "DescontoAdiconalOL")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.DescontoFinanceiro, "DescontoFinanceiro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.DescontoBonificadoVerba, "DescontoBonificadoVerba")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.QuantidadeUnidadesBonificadas, "QuantidadeUnidadesBonificadas")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.PorcentagemRepasse, "PorcentagemRepasse")
	if err != nil {
		return err
	}

	return err
}

var PosicoesProdutos = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                  {0, 1, 0},
	"CodigoBarras":                  {1, 15, 0},
	"CodigoProduto":                 {15, 23, 0},
	"ValorComST":                    {23, 31, 2},
	"Desconto":                      {31, 34, 2},
	"FormatoVendaProduto":           {34, 35, 0},
	"QuantidadeCaixa":               {35, 40, 0},
	"MinimoUnidades":                {40, 45, 0},
	"ValorMinimo":                   {45, 53, 2},
	"DescontoAdiconalOL":            {53, 56, 2},
	"DescontoFinanceiro":            {56, 59, 2},
	"DescontoBonificadoVerba":       {59, 62, 2},
	"QuantidadeUnidadesBonificadas": {62, 72, 0},
	"PorcentagemRepasse":            {72, 75, 2},
}
