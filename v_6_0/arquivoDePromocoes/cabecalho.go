package arquivoDePromocoes

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Cabecalho struct {
	TipoRegistro int32 `json:"TipoRegistro"`
	Cnpj         int64 `json:"Cnpj"`
}

func (c *Cabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalho

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.Cnpj, "Cnpj")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro": {0, 1, 0},
	"Cnpj":         {1, 15, 0},
}
