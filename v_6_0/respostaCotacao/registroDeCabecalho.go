package respostaCotacao

// Registro De Cabecalho
type RegistroDeCabecalho struct {
	TipoDeRegistro               string `json:"TipoDeRegistro"`
	CNPJ                         string `json:"CNPJ"`
	CodigoDeCotacao              int32  `json:"CodigoDeCotacao"`
	ValorMínimo                       int32  `json:"ValorMínimo"`
	DataFinalDeValidadeDosPrecos int32  `json:"DataFinalDeValidadeDosPrecos"`
	PrazoDePagamento             int32  `json:"PrazoDePagamento"`
}

func (r *RegistroDeCabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesRespostaCotacao.PosicoesRegistroDeCabecalho

	err = posicaoParaValor.ReturnByType(&r.TipoDeRegistro, "TipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.CNPJ, "CNPJ")
	err = posicaoParaValor.ReturnByType(&r.CodigoDeCotacao, "CodigoDeCotacao")
	err = posicaoParaValor.ReturnByType(&r.ValorMínimo, "ValorMínimo")
	err = posicaoParaValor.ReturnByType(&r.DataFinalDeValidadeDosPrecos, "DataFinalDeValidadeDosPrecos")
	err = posicaoParaValor.ReturnByType(&r.PrazoDePagamento, "PrazoDePagamento")

	return err
}
