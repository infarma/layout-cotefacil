package respostaCotacao


// Registro De Produtos
type RegistroDeProdutos struct {
	TipoDeRegistro                      string  `json:"TipoDeRegistro"`
	CodigoDeBarras                      string  `json:"CodigoDeBarras"`
	QuantidadeDisponivel                int32   `json:"QuantidadeDisponivel"`
	PrecoUnitarioComST                  int32   `json:"PrecoUnitarioComST"`
	EspacoEmBranco                      string  `json:"EspacoEmBranco"`
	Desconto                            int32   `json:"Desconto"`
	PrecoFabrica                        int32   `json:"PrecoFabrica"`
	TipoDeLista                         string  `json:"TipoDeLista"`
	ControleDePreco                     string  `json:"ControleDePreco"`
	TipoEmbalagem                       string  `json:"TipoEmbalagem"`
	QuantidadePorCaixa                  int32   `json:"QuantidadePorCaixa"`
	DescontoAdicional                   int32   `json:"DescontoAdicional"`
	CodigoDeFaturamentoQuandoEmPromocao string  `json:"CodigoDeFaturamentoQuandoEmPromocao"`
	DescontoFinanceiro                  int32   `json:"DescontoFinanceiro"`
	DescontoBonificadoEOuVerba          int32   `json:"DescontoBonificadoEOuVerba"`
	ProcentagemRepasse                  float32 `json:"ProcentagemRepasse"`
	MinimoUnidades                      int32   `json:"MinimoUnidades"`
}

func (r *RegistroDeProdutos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesRespostaCotacao.PosicoesRegistroDeProdutos

	err = posicaoParaValor.ReturnByType(&r.TipoDeRegistro, "TipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.CodigoDeBarras, "CodigoDeBarras")
	err = posicaoParaValor.ReturnByType(&r.QuantidadeDisponivel, "QuantidadeDisponivel")
	err = posicaoParaValor.ReturnByType(&r.PrecoUnitarioComST, "PrecoUnitarioComST")
	err = posicaoParaValor.ReturnByType(&r.EspacoEmBranco, "EspacoEmBranco")
	err = posicaoParaValor.ReturnByType(&r.Desconto, "Desconto")
	err = posicaoParaValor.ReturnByType(&r.PrecoFabrica, "PrecoFabrica")
	err = posicaoParaValor.ReturnByType(&r.TipoDeLista, "TipoDeLista")
	err = posicaoParaValor.ReturnByType(&r.ControleDePreco, "ControleDePreco")
	err = posicaoParaValor.ReturnByType(&r.TipoEmbalagem, "TipoEmbalagem")
	err = posicaoParaValor.ReturnByType(&r.QuantidadePorCaixa, "QuantidadePorCaixa")
	err = posicaoParaValor.ReturnByType(&r.DescontoAdicional, "DescontoAdicional")
	err = posicaoParaValor.ReturnByType(&r.CodigoDeFaturamentoQuandoEmPromocao, "CodigoDeFaturamentoQuandoEmPromocao")
	err = posicaoParaValor.ReturnByType(&r.DescontoFinanceiro, "DescontoFinanceiro")
	err = posicaoParaValor.ReturnByType(&r.DescontoBonificadoEOuVerba, "DescontoBonificadoEOuVerba")
	err = posicaoParaValor.ReturnByType(&r.ProcentagemRepasse, "ProcentagemRepasse")
	err = posicaoParaValor.ReturnByType(&r.MinimoUnidades, "MinimoUnidades")

	return err
}
