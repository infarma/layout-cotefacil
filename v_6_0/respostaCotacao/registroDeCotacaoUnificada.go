package respostaCotacao

type RegistroDeCotacaoUnificada struct {
	TipoDeRegistro  string `json:"TipoDeRegistro"`
	CNPJ            string `json:"CNPJ"`
	Aprovado        string `json:"Aprovado"`
	Motivo          string `json:"Motivo"`
	LimiteDeCredito string `json:"LimiteDeCredito"`
}

