package posicoesRespostaCotacao

import "layout-cotefacil/posicoes"

var PosicoesRegistroDeCotacaoUnificada = map[string]posicoes.Posicao{
	"TipoDeRegistro": {0, 1},
	"CNPJ":           {2, 16},
	"Aprovado":       {17, 18},
	"Motivo":         {18, 20},
	"LimiteDeCredito": {21, 30},
}