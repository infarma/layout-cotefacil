package posicoesRespostaCotacao

import "layout-cotefacil/posicoes"

var PosicoesRegistroDeProdutos = map[string]posicoes.Posicao{
	"TipoDeRegistro":                      {0, 1},
	"CodigoDeBarras":                      {2, 16},
	"QuantidadeDisponivel":                {17, 24},
	"PrecoUnitarioComST":                  {25, 33},
	"EspacoEmBranco":                      {34, 84},
	"Desconto":                            {85, 88},
	"PrecoFabrica":                        {89, 97},
	"TipoDeLista":                         {98, 99},
	"ControleDePreco":                     {100, 101},
	"TipoEmbalagem":                       {102, 103},
	"QuantidadePorCaixa":                  {104, 109},
	"DescontoAdicional":                   {110, 113},
	"CodigoDeFaturamentoQuandoEmPromocao": {114, 123},
	"DescontoFinanceiro":                  {124, 127},
	"DescontoBonificadoEOuVerba":          {128, 131},
	"ProcentagemRepasse":                  {128, 131},
	"MinimoUnidades":                      {131, 136},
}
