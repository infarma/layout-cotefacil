package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Cabecalho struct {
	TipoRegistro            int32  `json:"TipoRegistro"`
	CodigoCotacao           int64  `json:"CodigoCotacao"`
	CodigoPedidoCotefacil   int64  `json:"CodigoPedidoCotefacil"`
	CnpjCliente             string `json:"CnpjCliente"`
	CodigoFaturamento       string `json:"CodigoFaturamento"`
	CodigoCondicaoPagamento string `json:"CodigoCondicaoPagamento"`
	CnpjFornecedor          string `json:"CnpjFornecedor"`
}

func (c *Cabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalho

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoCotacao, "CodigoCotacao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoPedidoCotefacil, "CodigoPedidoCotefacil")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjCliente, "CnpjCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoFaturamento, "CodigoFaturamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoCondicaoPagamento, "CodigoCondicaoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjFornecedor, "CnpjFornecedor")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":            {0, 1, 0},
	"CodigoCotacao":           {1, 20, 0},
	"CodigoPedidoCotefacil":   {20, 39, 0},
	"CnpjFaturado":            {39, 53, 0},
	"CodigoFaturamento":       {53, 65, 0},
	"CodigoCondicaoPagamento": {65, 80, 0},
	"CnpjFornecedor":          {80, 94, 0},
}
