package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	Cabecalho Cabecalho  `json:"Cabecalho"`
	Produtos  []Produtos `json:"Produtos"`
	Rodape    Rodape     `json:"Rodape"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:1])

		var index int32
		if identificador == "1" {
			err = arquivo.Cabecalho.ComposeStruct(string(runes))
		} else if identificador == "2" {
			err = arquivo.Produtos[index].ComposeStruct(string(runes))
			index++
		} else if identificador == "3" {
			err = arquivo.Rodape.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}
