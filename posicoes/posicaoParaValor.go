package posicoes

import (
	"errors"
	"layout-cotefacil/utils"
	"reflect"
)

type PosicaoParaValor struct {
	FileContents string
	Posicoes     map[string]Posicao
}

func (p *PosicaoParaValor) ReturnContent(key string) (string, error) {
	start := p.Posicoes[key].Inicio
	end := p.Posicoes[key].Fim

	return p.FileContents[start:end], nil
}

func (p *PosicaoParaValor) ReturnAsInt32(key string) (int32, error) {
	value, err := p.ReturnAsInt64(key)

	return int32(value), err
}

func (p *PosicaoParaValor) ReturnAsInt64(key string) (int64, error) {
	valueAsString, err := p.ReturnContent(key)
	value, err := utils.StringToInt64(valueAsString)

	return value, err
}


func (p *PosicaoParaValor) ReturnAsString(key string) (string, error) {
	return p.ReturnContent(key)
}

func (p *PosicaoParaValor) ReturnByType(attribute interface{}, key string) error {
	var err error
	value := reflect.ValueOf(attribute).Elem()

	switch attribute.(type) {
	case *int:
		//	Não utilizado por enquanto
	case *int32:
		interval, err := p.ReturnAsInt32(key)
		value.Set(reflect.ValueOf(interval))
		return err
	case *int64:
		interval, err := p.ReturnAsInt64(key)
		value.Set(reflect.ValueOf(interval))
		return err
	case *string:
		interval, err := p.ReturnAsString(key)
		value.Set(reflect.ValueOf(interval))
		return err
	default:
		err = errors.New("erro ao converter tipo")
		return err
	}

	return err
}
