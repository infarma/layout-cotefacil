package remessaCotacao

import (
	"layout-cotefacil/v_1_1/remessaCotacao/posicoesRemessaCotacao"
	"layout-cotefacil/posicoes"
)

type RegistroDeCotacaoUnificada struct {
	TipoDeRegistro      string `json:"TipoDeRegistro"`
	CNPJ                string `json:"CNPJ"`
	UF                  string `json:"UF"`
	CodigoDeFaturamento int32  `json:"CodigoDeFaturamento"`
}

func (r *RegistroDeCotacaoUnificada) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesRemessaCotacao.PosicoesRegistroDeCotacaoUnificada

	err = posicaoParaValor.ReturnByType(&r.TipoDeRegistro, "TipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.CNPJ, "CNPJ")
	err = posicaoParaValor.ReturnByType(&r.UF, "UF")
	err = posicaoParaValor.ReturnByType(&r.CodigoDeFaturamento, "CodigoDeFaturamento")

	return err
}
