package posicoesRemessaCotacao

import "layout-cotefacil/posicoes"

var PosicoesRegistroDeProdutos = map[string]posicoes.Posicao{
	"TipoDeRegistro":  {0, 1},
	"CodigoDeBarras":  {2, 16},
	"Quantidade":      {17, 24},
	"ValorReferencia": {25, 33},
}
