package posicoesRemessaCotacao

import "layout-cotefacil/posicoes"

var PosicoesRegistroDeCabecalho = map[string]posicoes.Posicao{
	"TipoDeRegistro":                 {0, 1},
	"CNPJ":                           {2, 16},
	"UF":                             {17, 19},
	"DataDeEncerramento":             {20, 33},
	"CodigoDeCotacao":                {34, 44},
	"FormaDePagamento":               {45, 46},
	"Fixo":                           {47, 48},
	"CodigoDeFaturamento":            {49, 64},
	"DataInicialDeValidadeDosPrecos": {65, 73},
	"DataFinalDeValidadeDosPrecos":   {74, 82},
}
