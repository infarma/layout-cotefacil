package remessaCotacao

import (
	"layout-cotefacil/v_1_1/remessaCotacao/posicoesRemessaCotacao"
	"layout-cotefacil/posicoes"
)

// Registro De Produtos
type RegistroDeRodape struct {
	TipoDeRegistro       string `json:"TipoDeRegistro"`
	QuantidadeDeProdutos int32  `json:"QuantidadeDeProdutos"`
	VersaoDoLayout       string `json:"VersaoDoLayout"`
}

func (r *RegistroDeRodape) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesRemessaCotacao.PosicoesRegistroDeRodape

	err = posicaoParaValor.ReturnByType(&r.TipoDeRegistro, "TipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.QuantidadeDeProdutos, "QuantidadeDeProdutos")
	err = posicaoParaValor.ReturnByType(&r.VersaoDoLayout, "VersaoDoLayout")

	return err
}
