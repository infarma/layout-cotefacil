package posicoesArquivoDeAlteracaoDePrazo

import (
	"layout-cotefacil/v_1_1/aletracaoVencimentoCotacao/posicoesAlteracaoVencimentoCotacao"
	"layout-cotefacil/posicoes"
)

// Registro De Cabecalho
type PosicoesRegistroDeDataAtual struct {
	TipoDeRegistro string `json:"TipoDeRegistro"`
	DataAtual      int32  `json:"DataAtual"`
}

func (r *PosicoesRegistroDeDataAtual) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesAlteracaoVencimentoCotacao.PosicoesRegistroDeCabecalho

	err = posicaoParaValor.ReturnByType(&r.TipoDeRegistro, "TipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.DataAtual, "DataAtual")

	return err
}
