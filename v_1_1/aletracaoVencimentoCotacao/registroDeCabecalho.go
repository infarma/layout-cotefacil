package posicoesArquivoDeAlteracaoDePrazo

import (
	"layout-cotefacil/v_1_1/aletracaoVencimentoCotacao/posicoesAlteracaoVencimentoCotacao"
	"layout-cotefacil/posicoes"
)

// Registro De Cabecalho
type RegistroDeCabecalho struct {
	TipoDeRegistro  string `json:"TipoDeRegistro"`
	CodigoDeCotacao int32  `json:"CodigoDeCotacao"`
	CNPJ            string `json:"CNPJ"`
}

func (r *RegistroDeCabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesAlteracaoVencimentoCotacao.PosicoesRegistroDeCabecalho

	err = posicaoParaValor.ReturnByType(&r.TipoDeRegistro, "TipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.CodigoDeCotacao, "CodigoDeCotacao")
	err = posicaoParaValor.ReturnByType(&r.CNPJ, "CNPJ")

	return err
}
