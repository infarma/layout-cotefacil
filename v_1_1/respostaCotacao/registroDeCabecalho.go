package respostaCotacao

import (
	"layout-cotefacil/v_1_1/respostaCotacao/posicoesRespostaCotacao"
	"layout-cotefacil/posicoes"
)

// Registro De Cabecalho
type RegistroDeCabecalho struct {
	TipoDeRegistro               string `json:"TipoDeRegistro"`
	CNPJ                         string `json:"CNPJ"`
	CodigoDeCotacao              int32  `json:"CodigoDeCotacao"`
	Mínimo                       int32  `json:"Mínimo"`
	DataFinalDeValidadeDosPrecos int32  `json:"DataFinalDeValidadeDosPrecos"`
}

func (r *RegistroDeCabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesRespostaCotacao.PosicoesRegistroDeCabecalho

	err = posicaoParaValor.ReturnByType(&r.TipoDeRegistro, "TipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.CNPJ, "CNPJ")
	err = posicaoParaValor.ReturnByType(&r.CodigoDeCotacao, "CodigoDeCotacao")
	err = posicaoParaValor.ReturnByType(&r.Mínimo, "Mínimo")
	err = posicaoParaValor.ReturnByType(&r.DataFinalDeValidadeDosPrecos, "DataFinalDeValidadeDosPrecos")

	return err
}
