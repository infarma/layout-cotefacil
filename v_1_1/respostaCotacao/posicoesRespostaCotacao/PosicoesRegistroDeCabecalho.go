package posicoesRespostaCotacao


import "layout-cotefacil/posicoes"

var PosicoesRegistroDeCabecalho = map[string]posicoes.Posicao{
	"TipoDeRegistro":                 {0, 1},
	"CNPJ":                           {2, 16},
	"CodigoDeCotacao":                {17, 27},
	"Mínimo":                           {28, 36},
	"DataFinalDeValidadeDosPrecos":   {37, 45},
}