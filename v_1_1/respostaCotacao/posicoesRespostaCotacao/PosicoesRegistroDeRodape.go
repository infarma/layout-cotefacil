package posicoesRespostaCotacao

import "layout-cotefacil/posicoes"

var PosicoesRegistroDeRodape = map[string]posicoes.Posicao{
	"TipoDeRegistro":       {0, 1},
	"QuantidadeDeProdutos": {2, 6},
	"VersaoDoLayout":       {7, 10},
}
