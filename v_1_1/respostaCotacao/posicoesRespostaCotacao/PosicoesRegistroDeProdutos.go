package posicoesRespostaCotacao

import "layout-cotefacil/posicoes"

var PosicoesRegistroDeProdutos = map[string]posicoes.Posicao{
	"TipoDeRegistro":  {0, 1},
	"CodigoDeBarras":  {2, 16},
	"QuantidadeDisponivel":      {17, 24},
	"Preço": {25, 33},
	"MotivoDeAlteracao": {34, 84},
}