package respostaCotacao

import (
	"layout-cotefacil/v_1_1/respostaCotacao/posicoesRespostaCotacao"
	"layout-cotefacil/posicoes"
)

// Registro De Produtos
type RegistroDeProdutos struct {
	TipoDeRegistro       string  `json:"TipoDeRegistro"`
	CodigoDeBarras       string  `json:"CodigoDeBarras"`
	QuantidadeDisponível int32   `json:"QuantidadeDisponível"`
	Preco                float32 `json:"Preco"`
	MotivoDeAlteracao    int32   `json:"MotivoDeAlteracao"`
}

func (r *RegistroDeProdutos) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesRespostaCotacao.PosicoesRegistroDeProdutos

	err = posicaoParaValor.ReturnByType(&r.TipoDeRegistro, "TipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.CodigoDeBarras, "CodigoDeBarras")
	err = posicaoParaValor.ReturnByType(&r.QuantidadeDisponível, "QuantidadeDisponível")
	err = posicaoParaValor.ReturnByType(&r.Preco, "Preco")
	err = posicaoParaValor.ReturnByType(&r.MotivoDeAlteracao, "MotivoDeAlteracao")

	return err
}
