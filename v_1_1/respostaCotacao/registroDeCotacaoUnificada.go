package respostaCotacao

import (
	"layout-cotefacil/v_1_1/respostaCotacao/posicoesRespostaCotacao"
	"layout-cotefacil/posicoes"
)

type RegistroDeCotacaoUnificada struct {
	TipoDeRegistro string `json:"TipoDeRegistro"`
	CNPJ           string `json:"CNPJ"`
	Aprovado       string `json:"Aprovado"`
	Motivo         string `json:"Motivo"`
}

func (r *RegistroDeCotacaoUnificada) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = posicoesRespostaCotacao.PosicoesRegistroDeCotacaoUnificada

	err = posicaoParaValor.ReturnByType(&r.TipoDeRegistro, "TipoDeRegistro")
	err = posicaoParaValor.ReturnByType(&r.CNPJ, "CNPJ")
	err = posicaoParaValor.ReturnByType(&r.Aprovado, "Aprovado")
	err = posicaoParaValor.ReturnByType(&r.Motivo, "Motivo")

	return err
}
