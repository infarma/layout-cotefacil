package respostaCotacaoTest

import (
	"fmt"
	"layout-cotefacil/v_5_7/respostaCotacao"
	"testing"
	"time"
)

func TestCabecalhoResposta(t *testing.T){

	src := "2006-08-01T09:56:13.001Z"
	data, _ := time.Parse(time.RFC3339, src)
	cabecalho := respostaCotacao.CabecalhoDeResposta("76543218000188",5235, 150, data,10)

	if cabecalho != "1;76543218000188;5235;150;01082006;10" {
		fmt.Println("cabecalho", cabecalho)
		t.Error("Cabecalho  não é compativel")
	}
}

func TestProdutosResposta(t *testing.T){


	Produtos := respostaCotacao.ProdutosResposta("7896226500799",32,1.23," ",5.00,1.40,"0","L","C",5,2.00,"",2.00,0.00)

	if Produtos != "2;7896226500799;32;1.23; ;5.00;1.40;0;L;C;5;2.00;;2.00;0.00" {
		fmt.Println("Produtos", Produtos)
		t.Error("Produtos  não é compativel")
	}
}

func TestCotacaoResposta(t *testing.T){


	cotacao := respostaCotacao.CotacaoDeResposta("12345678000131","N","21",0)

	if cotacao != "4;12345678000131;N;21;0.00" {
		fmt.Println("cotacao", cotacao)
		t.Error("cotacao  não é compativel")
	}
}

func TestRodapeResposta(t *testing.T){


	rodape := respostaCotacao.RodapeDeResposta(2,"4.0")

	if rodape != "9;2;4.0" {
		fmt.Println("rodape", rodape)
		t.Error("rodape  não é compativel")
	}
}



