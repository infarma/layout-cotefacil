package arquivoDeAlteracaoDePrazoTest

import (
	"fmt"
	layout "layout-cotefacil/v_5_7/arquivoDeAlteracaoDePrazo"
	"testing"
	"time"
)

func TestCabecalho (t *testing.T){


	cabecalho := layout.CabecalhoAlteracao(5235, "12345678000134")
	if cabecalho != "1;5235;12345678000134;" {
		fmt.Println("data", cabecalho)
		t.Error("cabecalho de alteração de prazo é compativel")
	}
}

func TestDataAtual (t *testing.T){

	src2 := "2019-10-21T09:56:13.001Z"
	DataAtual, _ := time.Parse(time.RFC3339, src2)

	data := layout.DataAtualAlteracao(DataAtual)
	if data != "2;21102019 0956;" {
		fmt.Println("data", data)
		t.Error("Registro de Data Atual não é compativel")
	}
}

func TestNovaData (t *testing.T){

	src2 := "2019-10-21T09:56:13.001Z"
	NovaData, _ := time.Parse(time.RFC3339, src2)

	data := layout.NovaDataAlteracao(NovaData)
	if data != "3;21102019 0956;" {
		fmt.Println("data", data)
		t.Error("Registro de Nova Data não é compativel")
	}
}

func TestRodape (t *testing.T){

	rodape := layout.RodapeAlteracao("4.0")
	if rodape != "9;4.0;" {
		fmt.Println("rodape", rodape)
		t.Error("Rodape não é compativel")
	}
}


