package arquivoDePedidoTest

import (
	"layout-cotefacil/v_5_7/arquivoDePedido"
	"os"
	"testing"
)

func TestGetStruct(t *testing.T) {

	file, err := os.Open("../testFile/integraPedido1597_38100.txt")
	if err != nil {
		t.Error("não abriu arquivo", err)
	}

	arquivo, err2 := arquivoDePedido.GetStruct(file)
	//REGISTRO TIPO 1 ( cabecalho)
	var header arquivoDePedido.Cabecalho
	header.TipoRegistro = int32(1)
	header.CodigoCotacao = int64(1597)
	header.CodigoPedidoCotefacil = int64(38100)
	header.CnpjFaturado = "36463608000108"
	header.CodigoFaturamento = "0"
	header.PrazoPagamento = int32(28)

	if header.TipoRegistro != arquivo.Cabecalho.TipoRegistro {
		t.Error("TipoRegistro não é compativel", err2)
	}
	if header.CodigoCotacao != arquivo.Cabecalho.CodigoCotacao {
		t.Error("CodigoCotacao não é compativel", err2)
	}
	if header.CodigoPedidoCotefacil != arquivo.Cabecalho.CodigoPedidoCotefacil {
		t.Error("CodigoPedidoCotefacil não é compativel", err2)
	}
	if header.CnpjFaturado != arquivo.Cabecalho.CnpjFaturado {
		t.Error("CnpjFaturado não é compativel", err2)
	}
	if header.CodigoFaturamento != arquivo.Cabecalho.CodigoFaturamento {
		t.Error("CodigoFaturamento não é compativel", err2)
	}
	if header.PrazoPagamento != arquivo.Cabecalho.PrazoPagamento {
		t.Error("PrazoPagamento não é compativel", err2)
	}

	var produtos arquivoDePedido.Produtos
	produtos.TipoRegistro = int32(2)
	produtos.CodigoBarras = "7896014125241"
	produtos.Quantidade = int32(36)
	produtos.Preco = float32(1.99)
	produtos.CodigoFaturamentoQuandoPromocao = ""

	if produtos.TipoRegistro != arquivo.Produtos[0].TipoRegistro {
		t.Error("TipoRegistro não é compativel", err2)
	}
	if produtos.CodigoBarras != arquivo.Produtos[0].CodigoBarras {
		t.Error("CodigoBarras não é compativel", err2)
	}
	if produtos.Quantidade != arquivo.Produtos[0].Quantidade {
		t.Error("Quantidade não é compativel", err2)
	}
	if produtos.Preco != arquivo.Produtos[0].Preco {
		t.Error("Preco não é compativel", err2)
	}
	if produtos.CodigoFaturamentoQuandoPromocao != arquivo.Produtos[0].CodigoFaturamentoQuandoPromocao {
		t.Error("CodigoFaturamentoQuandoPromocao não é compativel", err2)
	}

	var rodape arquivoDePedido.Rodape
	rodape.TipoRegistro = int32(9)
	rodape.QuantidadeProdutos = int32(3)
	rodape.VersaoLayout  = "4.0"

	if rodape.TipoRegistro != arquivo.Rodape.TipoRegistro {
		t.Error("TipoRegistro não é compativel", err2)
	}
	if rodape.QuantidadeProdutos != arquivo.Rodape.QuantidadeProdutos {
		t.Error("QuantidadeProdutos não é compativel", err2)
	}
	if rodape.VersaoLayout != arquivo.Rodape.VersaoLayout {
		t.Error("VersaoLayout não é compativel", err2)
	}

}