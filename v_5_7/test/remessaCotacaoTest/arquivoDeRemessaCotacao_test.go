package remessaCotacaoTest

import (
	"fmt"
	"layout-cotefacil/v_5_7/remessaCotacao"
	"os"
	"testing"
	"time"
)

func TestGetStruct(t *testing.T) {

	file, err := os.Open("../testFile/COTACAO_5235.txt")
	if err != nil {
		t.Error("não abriu arquivo", err)
	}

	arquivo, err2 := remessaCotacao.GetStruct(file)
	//REGISTRO TIPO 1 ( cabecalho)
	var header remessaCotacao.RegistroDeCabecalho
	//1;12345678000134;SP;10102006 1547;5235;V;N;0;01112006;01122006;28

	header.TipoDeRegistro =  "1"
	header.CNPJ =  "12345678000134"
	header.UF =  "SP"
	data1 := time.Date(2006, 10, 10, 15, 47, 0, 0, time.UTC)
	header.DataDeEncerramento =  data1
	header.CodigoDeCotacao = int32(5235)
	header.FormaDePagamento = "V"
	header.Fixo = "N"
	header.CodigoDoCliente = int32(0)

	data2 := time.Date(2006, 11, 01, 0, 0, 0, 0, time.UTC)
	header.DataInicialDeValidadeDosPrecos = data2

	data3 := time.Date(2006, 12, 01, 0, 0, 0, 0, time.UTC)

	header.DataFinalDeValidadeDosPrecos = data3
	header.PrazoDePagamento = int32(28)

	if header.TipoDeRegistro != arquivo.RegistroDeCabecalho.TipoDeRegistro {
		t.Error("TipoDeRegistro não é compativel", err2)
	}
	if header.CNPJ != arquivo.RegistroDeCabecalho.CNPJ {
		t.Error("CNPJ não é compativel", err2)
	}
	if header.UF != arquivo.RegistroDeCabecalho.UF {
		t.Error("UF não é compativel", err2)
	}
	if header.DataDeEncerramento != arquivo.RegistroDeCabecalho.DataDeEncerramento {
		fmt.Println("data",arquivo.RegistroDeCabecalho.DataDeEncerramento )
		t.Error("DataDeEncerramento não é compativel", err2)
	}
	if header.CodigoDeCotacao != arquivo.RegistroDeCabecalho.CodigoDeCotacao {
		t.Error("CodigoDeCotacao não é compativel", err2)
	}
	if header.FormaDePagamento != arquivo.RegistroDeCabecalho.FormaDePagamento {
		t.Error("FormaDePagamento não é compativel", err2)
	}
	if header.Fixo != arquivo.RegistroDeCabecalho.Fixo {
		t.Error("Fixo não é compativel", err2)
	}
	if header.CodigoDoCliente != arquivo.RegistroDeCabecalho.CodigoDoCliente {
		t.Error("CodigoDoCliente não é compativel", err2)
	}
	if header.DataInicialDeValidadeDosPrecos != arquivo.RegistroDeCabecalho.DataInicialDeValidadeDosPrecos {
		t.Error("DataInicialDeValidadeDosPrecos não é compativel", err2)
	}
	if header.DataFinalDeValidadeDosPrecos != arquivo.RegistroDeCabecalho.DataFinalDeValidadeDosPrecos {
		t.Error("DataFinalDeValidadeDosPrecos não é compativel", err2)
	}
	if header.PrazoDePagamento != arquivo.RegistroDeCabecalho.PrazoDePagamento {
		t.Error("PrazoDePagamento não é compativel", err2)
	}

	var produtos remessaCotacao.RegistroDeProdutos
	//2;7896226500799;32;55.69
	produtos.TipoDeRegistro = "2"
	produtos.CodigoDeBarras = "7896226500799"
	produtos.Quantidade  = int32(32)
	produtos.ValorReferencia = float32(55.69)

	if produtos.TipoDeRegistro != arquivo.RegistroDeProdutos[0].TipoDeRegistro {
		t.Error("TipoDeRegistro não é compativel", err2)
	}
	if produtos.CodigoDeBarras != arquivo.RegistroDeProdutos[0].CodigoDeBarras {
		t.Error("CodigoDeBarras não é compativel", err2)
	}
	if produtos.Quantidade != arquivo.RegistroDeProdutos[0].Quantidade {
		t.Error("Quantidade não é compativel", err2)
	}
	if produtos.ValorReferencia != arquivo.RegistroDeProdutos[0].ValorReferencia {
		t.Error("ValorReferencia não é compativel", err2)
	}

	var cotacao remessaCotacao.RegistroDeCotacaoUnificada
	//4;12345678000137;SP;123456
	cotacao.TipoDeRegistro = "4"
	cotacao.CNPJ = "12345678000137"
	cotacao.UF = "SP"
	cotacao.CodigoDeFaturamento = int32(123456)

	if cotacao.TipoDeRegistro != arquivo.RegistroDeCotacaoUnificada[0].TipoDeRegistro {
		t.Error("TipoDeRegistro não é compativel", err2)
	}
	if cotacao.CNPJ != arquivo.RegistroDeCotacaoUnificada[0].CNPJ {
		t.Error("CNPJ não é compativel", err2)
	}
	if cotacao.UF != arquivo.RegistroDeCotacaoUnificada[0].UF {
		t.Error("UF não é compativel", err2)
	}
	if cotacao.CodigoDeFaturamento != arquivo.RegistroDeCotacaoUnificada[0].CodigoDeFaturamento {
		t.Error("CodigoDeFaturamento não é compativel", err2)
	}

	var rodape remessaCotacao.RegistroDeRodape
	//9;2;4.0
	rodape.TipoDeRegistro = "9"
	rodape.QuantidadeDeProdutos = int32(2)
	rodape.VersaoDoLayout  = "4.0"

	if rodape.TipoDeRegistro != arquivo.RegistroDeRodape.TipoDeRegistro {
		t.Error("TipoDeRegistro não é compativel", err2)
	}
	if rodape.QuantidadeDeProdutos != arquivo.RegistroDeRodape.QuantidadeDeProdutos {
		t.Error("QuantidadeDeProdutos não é compativel", err2)
	}
	if rodape.VersaoDoLayout != arquivo.RegistroDeRodape.VersaoDoLayout {
		t.Error("VersaoDoLayout não é compativel", err2)
	}

}
