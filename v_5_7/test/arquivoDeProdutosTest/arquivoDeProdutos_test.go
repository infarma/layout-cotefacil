package arquivoDeProdutosTest

import (
	"fmt"
	"layout-cotefacil/v_5_7/arquivoDeProdutos"
	"testing"
)

func TestCabecalhoProdutos(t *testing.T){


	cabecalho := arquivoDeProdutos.CabecalhoDeProdutos("88312457000182")

	if cabecalho != "1;88312457000182;" {
		t.Error("Cabecalho do produtos não é compativel")
	}
}

func TestProdutosDeProdutos(t *testing.T){


	produtos := arquivoDeProdutos.ProdutoDeProdutos("7702018913640","59327","DES GILLETTE 82GR POWER BEADS GEL COOL WAVE","GILLETTE DO BRASIL LTDA.", 1,"CAT A", "SUB B", "SUB C", "SUB D"  )

	if produtos != "2;7702018913640;59327;DES GILLETTE 82GR POWER BEADS GEL COOL WAVE;GILLETTE DO BRASIL LTDA.;1;CAT A;SUB B;SUB C;SUB D;" {
		fmt.Println("produtos", produtos)
		t.Error("Produtos de aquivo de produto não é compativel")
	}
}

func TestTotalizaçãoDeProdutos(t *testing.T){


	totalizacao := arquivoDeProdutos.TotalizacaoDeProdutos(4)

	if totalizacao != "3;4" {
		t.Error("totalizacao dos produtos não é compativel")
	}
}

func TestRodapeDeProdutos(t *testing.T){


	rodape := arquivoDeProdutos.RodapeDeProdutos(7)

	if rodape != "9;7" {
		t.Error("Rodape dos produtos não é compativel")
	}
}