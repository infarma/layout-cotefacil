package retornoDePedidoTest

import (
	"fmt"
	"layout-cotefacil/v_5_7/retornoDePedido"
	"testing"
)

func TestCabecalhoRetorno(t *testing.T){


	cabecalhoRetorno := retornoDePedido.CabecalhoRetorno("84962843000169", 1597, "S", 0, "38100", 30 )

	if cabecalhoRetorno != "1;84962843000169;1597;S;0;38100;30" {
		t.Error("Cabecalho do retorno não é compativel")
	}
}

func TestProdutoRetorno(t *testing.T){


	produtoRetorno := retornoDePedido.ProdutoRetorno("7896014125241", 2, 11.51, 0, 11.25 )

	if produtoRetorno != "2;7896014125241;2;11.51;0;11.25" {
		fmt.Println("retorno", produtoRetorno)
		t.Error("Produto do retorno não é compativel")
	}
}

func TestRodapeRetorno(t *testing.T){

	rodapeRetorno := retornoDePedido.RodapeRetorno(2,"4.0")

	if rodapeRetorno != "9;2;4.0" {
		t.Error("Cabecalho do retorno não é compativel")
	}
}
