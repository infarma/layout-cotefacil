package posicoesArquivoDeAlteracaoDePrazo

import (
	"fmt"
	"time"
)

// Cabeçalho
func CabecalhoAlteracao(CodigoDeCotacao int32, CNPJ string ) string {

	cabecalho := fmt.Sprint("1") + ";"
	cabecalho += fmt.Sprint(CodigoDeCotacao) + ";"
	cabecalho += fmt.Sprint(CNPJ) + ";"
	return cabecalho
}

// DataAtualAlteracao
func DataAtualAlteracao( DataAtual time.Time) string {

	dataAtual := fmt.Sprint("2") + ";"
	dataAtual += DataAtual.Format("02012006 1504") + ";"
	return dataAtual
}

// DataAtualAlteracao
func NovaDataAlteracao( NovaData time.Time) string {

	novaData := fmt.Sprint("3") + ";"
	novaData += NovaData.Format("02012006 1504") + ";"
	return novaData
}

// Rodape
func RodapeAlteracao( VersaoDoLayout string) string {

	rodape := fmt.Sprint("9") + ";"
	rodape += fmt.Sprint(VersaoDoLayout) + ";"
	return rodape
}

