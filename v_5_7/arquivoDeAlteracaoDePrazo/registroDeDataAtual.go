package posicoesArquivoDeAlteracaoDePrazo


// Registro De Cabecalho
type PosicoesRegistroDeDataAtual struct {
	TipoDeRegistro string `json:"TipoDeRegistro"`
	DataAtual      int32  `json:"DataAtual"`
}
