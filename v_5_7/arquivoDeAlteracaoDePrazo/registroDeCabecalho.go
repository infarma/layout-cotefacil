package posicoesArquivoDeAlteracaoDePrazo


// Registro De Cabecalho
type RegistroDeCabecalho struct {
	TipoDeRegistro  string `json:"TipoDeRegistro"`
	CodigoDeCotacao int32  `json:"CodigoDeCotacao"`
	CNPJ            string `json:"CNPJ"`
}

