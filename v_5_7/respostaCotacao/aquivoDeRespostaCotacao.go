package respostaCotacao

import (
	"fmt"
	"time"
)

// Cabeçalho
func CabecalhoDeResposta( CNPJ string, CodigoDeCotacao int32, Mínimo float32 , DataFinalDeValidadeDosPrecos time.Time, PrazoDePagamento int32) string {

	cabecalhoProdutos := fmt.Sprint("1") + ";"
	cabecalhoProdutos += fmt.Sprint(CNPJ) + ";"
	cabecalhoProdutos += fmt.Sprint(CodigoDeCotacao) + ";"
	cabecalhoProdutos += fmt.Sprint( Mínimo) + ";"
	cabecalhoProdutos += DataFinalDeValidadeDosPrecos.Format("02012006") + ";"
	cabecalhoProdutos += fmt.Sprint(PrazoDePagamento)

	return cabecalhoProdutos
}

// Produtos
func ProdutosResposta(	CodigoDeBarras string, QuantidadeDisponivel int32, PrecoUnitarioComST float32, EspacoEmBranco string, Desconto float32, PrecoFabrica float32, TipoDeLista string, ControleDePreco string, TipoEmbalagem string, QuantidadePorCaixa int32, DescontoAdicional float32, CodigoDeFaturamentoQuandoEmPromocao string, DescontoFinanceiro float32, DescontoBonificadoEOuVerba float32) string {

	produtosDeResposta := fmt.Sprint("2") + ";"
	produtosDeResposta += fmt.Sprint(CodigoDeBarras) + ";"
	produtosDeResposta += fmt.Sprint(QuantidadeDisponivel) + ";"
	produtosDeResposta += fmt.Sprintf("%.2f",PrecoUnitarioComST) + ";"
	produtosDeResposta += fmt.Sprint( EspacoEmBranco) + ";"
	produtosDeResposta += fmt.Sprintf("%.2f",Desconto) + ";"
	produtosDeResposta += fmt.Sprintf("%.2f",PrecoFabrica) + ";"
	produtosDeResposta += fmt.Sprint(TipoDeLista) + ";"
	produtosDeResposta += fmt.Sprint(ControleDePreco) + ";"
	produtosDeResposta += fmt.Sprint(TipoEmbalagem) + ";"
	produtosDeResposta += fmt.Sprint(QuantidadePorCaixa) + ";"
	produtosDeResposta += fmt.Sprintf("%.2f",DescontoAdicional) + ";"
	produtosDeResposta += fmt.Sprint(CodigoDeFaturamentoQuandoEmPromocao) + ";"
	produtosDeResposta += fmt.Sprintf("%.2f",DescontoFinanceiro) + ";"
	produtosDeResposta += fmt.Sprintf("%.2f",DescontoBonificadoEOuVerba)


	return 	produtosDeResposta
}

// Cotacao
func CotacaoDeResposta(CNPJ string, Aprovado string, Motivo string, LimiteDeCredito float32) string {

	cotacao := fmt.Sprint("4") + ";"
	cotacao += fmt.Sprint(CNPJ) + ";"
	cotacao += fmt.Sprint(Aprovado) + ";"
	cotacao += fmt.Sprint(Motivo) + ";"
	cotacao += fmt.Sprintf("%.2f",LimiteDeCredito)

	return cotacao
}

// Rodape
func RodapeDeResposta(QtdeProdutos int32, VersaoLayout string) string {

	rodape := fmt.Sprint("9") + ";"
	rodape += fmt.Sprint(QtdeProdutos) + ";"
	rodape += fmt.Sprint(VersaoLayout)
	return rodape
}
