package respostaCotacao
// Registro De Produtos
type RegistroDeProdutos struct {
	TipoDeRegistro                      string `json:"TipoDeRegistro"`
	CodigoDeBarras                      string `json:"CodigoDeBarras"`
	QuantidadeDisponivel                int32  `json:"QuantidadeDisponivel"`
	PrecoUnitarioComST                  float32  `json:"PrecoUnitarioComST"`
	EspacoEmBranco                      string `json:"EspacoEmBranco"`
	Desconto                            float32  `json:"Desconto"`
	PrecoFabrica                        float32  `json:"PrecoFabrica"`
	TipoDeLista                         string `json:"TipoDeLista"`
	ControleDePreco                     string `json:"ControleDePreco"`
	TipoEmbalagem                       string `json:"TipoEmbalagem"`
	QuantidadePorCaixa                  int32  `json:"QuantidadePorCaixa"`
	DescontoAdicional                   float32  `json:"DescontoAdicional"`
	CodigoDeFaturamentoQuandoEmPromocao string `json:"CodigoDeFaturamentoQuandoEmPromocao"`
	DescontoFinanceiro                  float32  `json:"DescontoFinanceiro"`
	DescontoBonificadoEOuVerba          float32  `json:"DescontoBonificadoEOuVerba"`
}
