package respostaCotacao

import "time"

// Registro De Cabecalho
type RegistroDeCabecalho struct {
	TipoDeRegistro               string `json:"TipoDeRegistro"`
	CNPJ                         string `json:"CNPJ"`
	CodigoDeCotacao              int32  `json:"CodigoDeCotacao"`
	Mínimo                       float32  `json:"Mínimo"`
	DataFinalDeValidadeDosPrecos time.Time  `json:"DataFinalDeValidadeDosPrecos"`
	PrazoDePagamento             int32  `json:"PrazoDePagamento"`
}
