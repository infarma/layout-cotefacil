package arquivoDePedido

type Produtos struct {
	TipoRegistro                    int32   `json:"TipoRegistro"`
	CodigoBarras                    string  `json:"CodigoBarras"`
	Quantidade                      int32   `json:"Quantidade"`
	Preco                           float32 `json:"Preco"`
	CodigoFaturamentoQuandoPromocao string  `json:"CodigoFaturamentoQuandoPromocao"`
}
