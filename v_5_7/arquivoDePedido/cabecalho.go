package arquivoDePedido

type Cabecalho struct {
	TipoRegistro          int32  `json:"TipoRegistro"`
	CodigoCotacao         int64  `json:"CodigoCotacao"`
	CodigoPedidoCotefacil int64  `json:"CodigoPedidoCotefacil"`
	CnpjFaturado          string `json:"CnpjFaturado"`
	CodigoFaturamento     string `json:"CodigoFaturamento"`
	PrazoPagamento        int32  `json:"PrazoPagamento"`
}
