package arquivoDePedido

type Rodape struct {
	TipoRegistro       int32  `json:"TipoRegistro"`
	QuantidadeProdutos int32  `json:"QuantidadeProdutos"`
	VersaoLayout       string `json:"VersaoLayout"`
}

