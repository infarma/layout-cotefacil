package arquivoDePedido

import (
	"bufio"
	"math"
	"os"
	"strconv"
	"strings"
)

type ArquivoDePedido struct {
	Cabecalho Cabecalho  `json:"Cabecalho"`
	Produtos  []Produtos `json:"Produtos"`
	Rodape    Rodape     `json:"Rodape"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		vector := strings.Split(string(runes), ";")
		identificador := int32(ConvertStringToInt(vector[0]))

		if identificador == 1 {

			header := Cabecalho{}
			header.TipoRegistro =  int32(ConvertStringToInt(vector[0]))
			header.CodigoCotacao =  int64(ConvertStringToInt(vector[1]))
			header.CodigoPedidoCotefacil =  int64(ConvertStringToInt(vector[2]))
			header.CnpjFaturado = vector[3]
			header.CodigoFaturamento = vector[4]
			header.PrazoPagamento =  int32(ConvertStringToInt(vector[5]))

			arquivo.Cabecalho = header
		} else if identificador == 2 {

			produtos := Produtos{}
			produtos.TipoRegistro = int32(ConvertStringToInt(vector[0]))
			produtos.CodigoBarras = vector[1]
			produtos.Quantidade = int32(ConvertStringToInt(vector[2]))

			precoFloat := strings.Split(vector[3], ".")
			PrecoInteiro := float32(ConvertStringToInt(precoFloat[0]))
			PrecoDecimal := (float32(ConvertStringToInt(precoFloat[1])))/float32(math.Pow10(len(precoFloat[1])))
			produtos.Preco = PrecoInteiro + PrecoDecimal

			produtos.CodigoFaturamentoQuandoPromocao = vector[4]

			arquivo.Produtos = append(arquivo.Produtos, produtos)

		} else if identificador == 9 {

			rodape := Rodape{}
			rodape.TipoRegistro = int32(ConvertStringToInt(vector[0]))
			rodape.QuantidadeProdutos = int32(ConvertStringToInt(vector[1]))
			rodape.VersaoLayout  = vector[2]

			arquivo.Rodape = rodape

		}
	}
	return arquivo, err
}

func ConvertStringToInt(valor string) int {
	v, _ := strconv.Atoi(valor)
	return v
}