package arquivoDeProdutos

type Produtos struct {
	TipoRegistro         int32  `json:"TipoRegistro"`
	CodigoBarras         string `json:"CodigoBarras"`
	CodigoProduto        string `json:"CodigoProduto"`
	NomeProduto          string `json:"NomeProduto"`
	NomeFabricante       string `json:"NomeFabricante"`
	SituacaoProduto      int32  `json:"SituacaoProduto"`
	CategoriaProduto     string `json:"CategoriaProduto"`
	Subcategoria1Produto string `json:"Subcategoria1Produto"`
	Subcategoria2Produto string `json:"Subcategoria2Produto"`
	Subcategoria3Produto string `json:"Subcategoria3Produto"`
}

