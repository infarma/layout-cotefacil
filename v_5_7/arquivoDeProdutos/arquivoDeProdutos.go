package arquivoDeProdutos

import "fmt"

// Cabeçalho de Produtos
func CabecalhoDeProdutos( Cnpj string) string {

	cabecalhoProdutos := fmt.Sprint("1") + ";"
	cabecalhoProdutos += fmt.Sprint(Cnpj) + ";"

	return cabecalhoProdutos
}

// Produtos de Produtos
func ProdutoDeProdutos(	CodigoBarras string, CodigoProduto string, NomeProduto string, NomeFabricante string, SituacaoProduto int32, CategoriaProduto string, Subcategoria1Produto string, Subcategoria2Produto string, Subcategoria3Produto string ) string {

	produtoProdutos := fmt.Sprint("2") + ";"
	produtoProdutos += fmt.Sprint(CodigoBarras) + ";"
	produtoProdutos += fmt.Sprint(CodigoProduto) + ";"
	produtoProdutos += fmt.Sprint(NomeProduto) + ";"
	produtoProdutos += fmt.Sprint(NomeFabricante) + ";"
	produtoProdutos += fmt.Sprint(SituacaoProduto) + ";"
	produtoProdutos += fmt.Sprint(CategoriaProduto) + ";"
	produtoProdutos += fmt.Sprint(Subcategoria1Produto) + ";"
	produtoProdutos += fmt.Sprint(Subcategoria2Produto) + ";"
	produtoProdutos += fmt.Sprint(Subcategoria3Produto) + ";"

	return produtoProdutos
}

// Totalização de Produtos
func TotalizacaoDeProdutos( QuantidadeProdutos int32) string {

	TotalizacaoProdutos := fmt.Sprint("3") + ";"
	TotalizacaoProdutos += fmt.Sprint(QuantidadeProdutos)

	return TotalizacaoProdutos
}

// Rodape de Produtos
func RodapeDeProdutos( QuantidadeTotalProdutos int32) string {

	rodapeDeProdutos := fmt.Sprint("9") + ";"
	rodapeDeProdutos += fmt.Sprint(QuantidadeTotalProdutos)
	return rodapeDeProdutos
}