package arquivoDeProdutos

type TotalizacaoProdutos struct {
	TipoRegistro       int32 `json:"TipoRegistro"`
	QuantidadeProdutos int32 `json:"QuantidadeProdutos"`
}
