package arquivoDeProdutos

type Rodape struct {
	TipoRegistro            int32 `json:"TipoRegistro"`
	QuantidadeTotalProdutos int32 `json:"QuantidadeTotalProdutos"`
}

