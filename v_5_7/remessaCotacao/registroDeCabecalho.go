package remessaCotacao

import "time"

// Registro De Cabecalho
type RegistroDeCabecalho struct {
	TipoDeRegistro                 string 	  `json:"TipoDeRegistro"`
	CNPJ                           string 	  `json:"CNPJ"`
	UF                             string 	  `json:"UF"`
	DataDeEncerramento             time.Time  	  `json:"DataDeEncerramento"`
	CodigoDeCotacao                int32  	  `json:"CodigoDeCotacao"`
	FormaDePagamento               string 	  `json:"FormaDePagamento"`
	Fixo                           string 	  `json:"Fixo"`
	CodigoDoCliente                int32  	  `json:"CodigoDoCliente"`
	DataInicialDeValidadeDosPrecos time.Time  `json:"DataInicialDeValidadeDosPrecos"`
	DataFinalDeValidadeDosPrecos   time.Time  `json:"DataFinalDeValidadeDosPrecos"`
	PrazoDePagamento               int32      `json:"PrazoDePagamento"`
}

