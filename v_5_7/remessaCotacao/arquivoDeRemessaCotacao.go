package remessaCotacao
//TODO: verificar se o registro remessaCotacao é pra ser lido ou gerado pelo layout
//TODO: alterar as variaveis datas para time.Time

import (
	"bufio"
	"math"
	"os"
	"strconv"
	"strings"
	"time"
)

type ArquivoDeRemessaCotacao struct {

	RegistroDeCabecalho RegistroDeCabecalho                  `json:"RegistroDeCabecalho"`
	RegistroDeCotacaoUnificada  []RegistroDeCotacaoUnificada `json:"RegistroDeCotacaoUnificada"`
	RegistroDeProdutos  []RegistroDeProdutos                 `json:"RegistroDeProdutos"`
	RegistroDeRodape  RegistroDeRodape                       `json:"RegistroDeRodape"`

}

func GetStruct(fileHandle *os.File) (ArquivoDeRemessaCotacao, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDeRemessaCotacao{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		vector := strings.Split(string(runes), ";")
		identificador := int32(ConvertStringToInt(vector[0]))

		if identificador == 1 {

			header := RegistroDeCabecalho{}
			header.TipoDeRegistro =  vector[0]
			header.CNPJ =  vector[1]
			header.UF =  vector[2]
			header.CodigoDeCotacao = int32(ConvertStringToInt(vector[4]))
			header.FormaDePagamento = vector[5]
			header.Fixo = vector[6]
			header.CodigoDoCliente = int32(ConvertStringToInt(vector[7]))

			// convertendo a string para date-----------------------------------------------------------------
			//10102006
			encerramento := strings.Split(string(vector[3]), " ")
			diaEncerramento := int(ConvertStringToInt(encerramento[0][0:2]))
			mesEncerramento := int(ConvertStringToInt(encerramento[0][2:4]))
			anoEncerramento := int(ConvertStringToInt(encerramento[0][4:8]))
			horasEncerramento := int(ConvertStringToInt(encerramento[1][0:2]))
			minutosEncerramento := int(ConvertStringToInt(encerramento[1][2:4]))
			dataEncerramento := time.Date(anoEncerramento, time.Month(mesEncerramento), diaEncerramento, horasEncerramento, minutosEncerramento, 0, 0, time.UTC)
			header.DataDeEncerramento = dataEncerramento


			diaIni := int(ConvertStringToInt(vector[8][0:2]))
			mesIni := int(ConvertStringToInt(vector[8][2:4]))
			anoIni := int(ConvertStringToInt(vector[8][4:8]))
			dataIni := time.Date(anoIni, time.Month(mesIni), diaIni, 0, 0, 0, 0, time.UTC)
			header.DataInicialDeValidadeDosPrecos = dataIni

			diaFim := int(ConvertStringToInt(vector[9][0:2]))
			mesFim := int(ConvertStringToInt(vector[9][2:4]))
			anoFim := int(ConvertStringToInt(vector[9][4:8]))
			dataFim := time.Date(anoFim, time.Month(mesFim), diaFim, 0, 0, 0, 0, time.UTC)
			header.DataFinalDeValidadeDosPrecos = dataFim
			// ---------------------------------------------------------------------------

			header.PrazoDePagamento = int32(ConvertStringToInt(vector[10]))

			arquivo.RegistroDeCabecalho = header
		} else if identificador == 4 {

			cotacao := RegistroDeCotacaoUnificada{}
			cotacao.TipoDeRegistro = vector[0]
			cotacao.CNPJ = vector[1]
			cotacao.UF = vector[2]
			cotacao.CodigoDeFaturamento = int32(ConvertStringToInt(vector[3]))

			arquivo.RegistroDeCotacaoUnificada = append(arquivo.RegistroDeCotacaoUnificada, cotacao)

		} else if identificador == 2 {

			produtos := RegistroDeProdutos{}
			produtos.TipoDeRegistro = vector[0]
			produtos.CodigoDeBarras = vector[1]
			produtos.Quantidade  = int32(ConvertStringToInt(vector[2]))

			// coverter string com . para float
			valorReferenciavector := strings.Split(vector[3], ".")
			valorReferenciaInteiro := float32(ConvertStringToInt(valorReferenciavector[0]))
			if(len(valorReferenciavector) > 1) {
				valorReferenciaDecimal := (float32(ConvertStringToInt(valorReferenciavector[1])))/float32(math.Pow10(len(valorReferenciavector[1])))
				produtos.ValorReferencia = valorReferenciaInteiro + valorReferenciaDecimal
			} else{
				produtos.ValorReferencia = valorReferenciaInteiro
			}

			arquivo.RegistroDeProdutos = append(arquivo.RegistroDeProdutos, produtos)

		}  else if identificador == 9 {

			rodape := RegistroDeRodape{}
			rodape.TipoDeRegistro = vector[0]
			rodape.QuantidadeDeProdutos = int32(ConvertStringToInt(vector[1]))
			rodape.VersaoDoLayout  = vector[2]

			arquivo.RegistroDeRodape = rodape

		}
	}
	return arquivo, err
}

func ConvertStringToInt(valor string) int {
	v, _ := strconv.Atoi(valor)
	return v
}