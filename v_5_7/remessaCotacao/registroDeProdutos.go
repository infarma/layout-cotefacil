package remessaCotacao

// Registro De Produtos
type RegistroDeProdutos struct {
	TipoDeRegistro  string `json:"TipoDeRegistro"`
	CodigoDeBarras  string `json:"CodigoDeBarras"`
	Quantidade      int32  `json:"Quantidade"`
	ValorReferencia float32  `json:"ValorReferencia"`
}
