package retornoDePedido

type Produtos struct {
	TipoRegistro       int32  	`json:"TipoRegistro"`
	CodigoBarras       string 	`json:"CodigoBarras"`
	QuantidadeFaturada int64  	`json:"QuantidadeFaturada"`
	PrecoComST         float32	`json:"PrecoComST"`
	Motivo             int32  	`json:"Motivo"`
	PrecoSemST         float32	`json:"PrecoSemST"`
}
