package retornoDePedido

type Cabecalho struct {
	TipoRegistro     int32 	`json:"TipoRegistro"`
	Cnpj             string	`json:"Cnpj"`
	CodigoReferencia int64 	`json:"CodigoReferencia"`
	Faturado         string	`json:"Faturado"`
	Motivo           int32 	`json:"Motivo"`
	CodigoPedido     string	`json:"CodigoPedido"`
	PrazoPagamento   int32 	`json:"PrazoPagamento"`
}
