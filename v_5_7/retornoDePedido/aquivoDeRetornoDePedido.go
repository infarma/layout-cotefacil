package retornoDePedido

import (
	"fmt"
)


// Cabeçalho do Retorno do Pedido de Compra
func CabecalhoRetorno( Cnpj string, CodigoReferencia int64, Faturado string, Motivo int32, CodigoPedido string, PrazoPagamento int32) string {

	cabecalhoRetorno := fmt.Sprint("1") + ";"
	cabecalhoRetorno += fmt.Sprint(Cnpj) + ";"
	cabecalhoRetorno += fmt.Sprint(CodigoReferencia) + ";"
	cabecalhoRetorno += fmt.Sprint(Faturado) + ";"
	cabecalhoRetorno += fmt.Sprint(Motivo) +  ";"
	cabecalhoRetorno += fmt.Sprint(CodigoPedido) +  ";"
	cabecalhoRetorno += fmt.Sprint(PrazoPagamento)

	return cabecalhoRetorno
}

// Produto do Retorno do Pedido de Compra
func ProdutoRetorno(CodigoBarras string, QuantidadeFaturada int64, PrecoComST float32, Motivo int32, PrecoSemST float32) string {

	produtoRetorno := fmt.Sprint("2") + ";"
	produtoRetorno += fmt.Sprint(CodigoBarras) + ";"
	produtoRetorno += fmt.Sprint(QuantidadeFaturada) + ";"
	produtoRetorno += fmt.Sprint(PrecoComST) + ";"
	produtoRetorno += fmt.Sprint(Motivo) + ";"
	produtoRetorno += fmt.Sprintf("%.2f",PrecoSemST)
	return produtoRetorno
}

// Rodapé do Retorno do Pedido de Compra
func RodapeRetorno(QuantidadeProdutos int32, VersaoLayout string)string {

	rodapeRetorno := fmt.Sprint("9") + ";"
	rodapeRetorno += fmt.Sprint(QuantidadeProdutos) + ";"
	rodapeRetorno += fmt.Sprint(VersaoLayout)

	return rodapeRetorno
}


